from operator import sub
from pyexpat import model
from statistics import mode
from unicodedata import category
from django.db import models
from datetime import datetime

# Create your models here.
class AndroidApps(models.Model):
    category_choices = (
        ("Entataiment", "Entataiment"),
        ("Games", "Games"),
    )
    sub_category_choices = (
        ("Social", "Social"),
        ("Arcade", "Arcade"),
    )
    app_name = models.CharField(max_length=100, default=False)
    play_store_id = models.CharField(max_length=255, default=False)
    category = models.CharField(choices=category_choices, max_length=255)
    sub_category = models.CharField(choices=sub_category_choices, max_length=255)
    cdate = models.DateTimeField(blank=True, default=datetime.now)
